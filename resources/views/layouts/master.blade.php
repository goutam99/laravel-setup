<!DOCTYPE html>
<html lang="en">
<head>

@include('partial.head')

@stack('top')

</head>
<body>
<!-- header elements -->
@include('partial.header')
<!-- header elements -->
<!-- Container -->
@yield('content')	 

@include('partial.footer')

@include('partial.scripts')

@stack('bottom')

</body>
</html>
