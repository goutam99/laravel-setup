@extends('crudbooster::admin_template')

@section('content')

    <div class="box">
        <div class="box-body table-responsive no-padding">
            <table id='table' class="table table-hover table-striped table-bordered">
            	<thead>
        			<tr class="active">
        				<th width='auto' style="text-align:left">Name</th>
        				<th width='auto' style="text-align:right">Actions</th>
        			</tr>
        		</thead>
        		<tbody>
        			@forelse($settings as $setting)
	        			<tr>
	        				<td>{{ $setting }}</td>
	        				<td width="auto" style="text-align:right">
	        					<a class="btn btn-sm btn-success" href="{{route("AdminManageSettingsControllerGetShow")}}?group={{urlencode($setting)}}"><i class="fa fa-edit"></i></a>
	        				</td>
	        			</tr>
        			@empty

        			@endforelse
        		</tbody>
            </table>
        </div>
    </div>

@endsection



@push('bottom')
    <script type="text/javascript">
        $(document).ready(function () {
            var $window = $(window);

            function checkWidth() {
                var windowsize = $window.width();
                if (windowsize > 500) {
                    console.log(windowsize);
                    $('#box-body-table').removeClass('table-responsive');
                } else {
                    console.log(windowsize);
                    $('#box-body-table').addClass('table-responsive');
                }
            }

            checkWidth();
            $(window).resize(checkWidth);

            $('.selected-action ul li a').click(function () {
                var name = $(this).data('name');
                $('#form-table input[name="button_name"]').val(name);
                var title = $(this).attr('title');

                swal({
                        title: "{{trans("crudbooster.confirmation_title")}}",
                        text: "{{trans("crudbooster.alert_bulk_action_button")}} " + title + " ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#008D4C",
                        confirmButtonText: "{{trans('crudbooster.confirmation_yes')}}",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function () {
                        $('#form-table').submit();
                    });

            })

            $('table tbody tr .button_action a').click(function (e) {
                e.stopPropagation();
            })
        });
    </script>
@endpush