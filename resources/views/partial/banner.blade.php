@if(!empty($sliders) && count($sliders)>0)
<section class="bannercontainer">
  <div id="HomeBanner" class="owl-carousel owl-theme">
    @foreach($sliders as $slider)
    <div class="item">
      <img class="owl-lazy" data-src="{{ asset($slider->image_path) }}" alt="{{ $slider->title }}">
    </div>
    @endforeach    
  </div>
</section>
@endif