<header class="headercontainer">
  <div class="headerinner">
  	<div class="topheadersocial">
	    <div class="container">
	      <div class="row">
	        <div class="col-12">
	          <div class="topheadersocialinner align-items-center">
	          	<div class="logocontainer">
  	            <a href="{{ url('/') }}" title="Codes Tutors"><img src="{{ asset('assets/images/logo-codestutors.png') }}" alt="Codes Tutors"></a>
  	          </div>
	          	<div class="usersearchbox" id="SearchBox">
	          	    <form action="{{url('search')}}">
	          		<div class="usersearchboxinner">
		          		<div class="input-group">
                            <input type="text" class="form-control" name="q" placeholder="Enter Course, Category or keyword" value="{{ request()->get('q') }}" />
                            <div class="input-group-append">
                            <button type="submit" class="btn searchbtn"><i class="icon icon-search"></i></button>
                            </div>
                        </div>
                    </div>
                    </form>
	          	</div>
	          	<div class="ml-auto">
	  	          <div class="userboxright">	                
	                <a href="javascript:void(0);" title="Projects"><span>Projects</span></a>
	                <a href="javascript:void(0);" title="Blog"><span>Blog</span></a>
	                <a class="login" href="javascript:void(0);" title="Login"><span>Login</span></a>
	                <a class="signup" href="javascript:void(0);" title="Sign Up"><span>Sign Up</span></a>
	              </div>
                <a class="searchbtnicon" id="SearchIcon" href="javascript:void(0);" title="Codes Tutors"><i class="icon icon-search"></i></a>
	              <a class="NavBar" href="javascript:void(0);" title="Codes Tutors"><i class="icon icon-segment"></i></a>
	            </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
	  <div class="btmheader">
      <div class="btmheaderinner">
  	    <div class="container">
  	      <div class="headtopouter">
  	        <div class="headtoprow">
  	          <div class="navigationcolumn">
  	            <div class="navigation">
  	              <div class="navigationbox">
					@if(!empty($data['categorys']))
  	                <ul class="sf-menu">
					  @foreach($data['categorys'] as $category)
  	                  <li class="active"><a href="{{ url('tutorials/'.$category->category_slug.'/'.$category->subcategory->first()->category_slug) }}" title="{{ $category->category_name }}">{{ $category->category_name }}</a></li>  	                  
					  @endforeach
                    </ul>
					@endif
  	              </div>
  	            </div>
                <!-- <div class="navbaricon">
                  <a id="NavIcon" href="javascript:void(0);"><i class="icon icon-menu1"></i></a>
                  <div id="NavBox" class="navmenudrop">
                    <ul>
                      <li><a href="javascript:void(0);">Programming & Frameworks</a></li>
                      <li><a href="javascript:void(0);">Frontend Development</a></li>
                      <li><a href="javascript:void(0);">Robotic Process Automation</a></li>
                      <li><a href="javascript:void(0);">Digital Marketing</a></li>
                      <li><a href="javascript:void(0);">Data Warehousing and ETL</a></li>
                      <li><a href="javascript:void(0);">Artificial Intelligence</a></li>
                      <li><a href="javascript:void(0);">Cyber Security</a></li>
                      <li><a href="javascript:void(0);">Mobile Development</a></li>
                      <li><a href="javascript:void(0);">Databases</a></li>
                      <li><a href="javascript:void(0);">Operating Systems</a></li>
                      <li><a href="javascript:void(0);">Blockchain</a></li>
                      <li><a href="javascript:void(0);">Architecture & Design Patterns</a></li>
                    </ul>
                  </div>
                </div> -->
  	          </div>
  	        </div>
  	      </div>
  	    </div>
      </div>
	  </div>
  </div>
</header>