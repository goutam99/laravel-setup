
<script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.lazy.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha512-YUkaLm+KJ5lQXDBdqBqk7EVhJAdxRnVdT2vtCzwPHSweCzyMgYV/tgGF4/dCyqtCC2eCphz0lRQgatGVdfR0ww==" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready( function(){
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    });


	jQuery(document).ready(function($) {
		let $subscribeForm = $('#NewsletterForm');
		/*jQuery.validator.setDefaults({
		  debug: true,
		  success: "valid"
		});*/

		$subscribeForm.validate({
			errorElement: "span",
			rules: {								
				email: {
					required: true,
					normalizer: function(value) {
						return $.trim(value);
					}
				}				
			},
            messages:{
                email: {
                    required: "Please enter your email."
                }                
            }	
		});


		$subscribeForm.ajaxForm({
			dataType: "JSON",
			beforeSubmit: function(formData, jqForm, options){
				if(!jqForm.valid()) return false
				$subscribeForm.find('span.error').html('')				
				$subscribeForm.find('button[type=submit]').attr('disabled', true);
                $("#result-message").html('Loading...');
			},
			success: function(response){				
				$subscribeForm.trigger("reset");
				$subscribeForm.find('button[type=submit]').removeAttr('disabled');	
                $("#result-message").html(response.message);			
			},
			error: function(XMLHttpRequest, textStatus, errorThrown){
				toastr.error("Some error occurred, kindly try again later.")
				$subscribeForm.find('button[type=submit]').removeAttr('disabled');
			}
		})
	});
</script>

<script type="text/javascript">
    $(document).ready( function(){
        $(".valid_number").on("keypress keyup blur",function (e) {
            $(this).val($(this).val().replace(/^[a-zA-Z]+$/, ""));
                if ((e.which < 48 || e.which > 57)) {
                    e.preventDefault();
            }
        });
      });
</script>