
<footer class="footercontainer">
  <div class="footertop">
    <div class="container">
      <div class="row">
        <div class="col-xl-3 col-md-6 col-sm-6 col-12 mb-4 mt-2">
          <div class="footerlogobox">
            <a href="index.html"><img src="{{ asset('assets/images/logo-codestutors.png') }}" alt="Footer Logo" /></a>
          </div>
          {!! config('settings.footer_text') !!}
          <a href="{{ url('about-us') }}" class="readmore">Read more +</a>
        </div>
        <div class="col-xl-3 col-md-6 col-sm-6 col-12 mb-4 mt-2">
          <div class="footernav">
            <h3>Quick Links</h3>
            <ul>
              <li class="active"><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('about-us') }}">About Us</a></li>              
              <li><a href="javascript:void(0);">Projects</a></li>                                               
              <li><a href="javascript:void(0);">Blog</a></li>                                               
              <li><a href="javascript:void(0);">Contact Us</a></li>
            </ul>
          </div>
        </div>
        <div class="col-xl-3 col-md-6 col-sm-6 col-12 mb-4 mt-2">
          <div class="footercontaintinfo">
            <h3>Newsletter</h3>            
            <form action="{{ url('subscribe-newsletter') }}" id="NewsletterForm" method="post">
                <div id="message-nf"></div>
                <div class="form-group">
                  <input type="email" name="email" id="nf-email" class="form-control" placeholder="Enter email for newsletter">
                  <span class="error" id="newsletter-err"></span>
                  <p id="result-message"></p>
                </div>
                <div class="form-group">
                  <input type="submit" class="btn btn-custom" value="Subscribe">
                </div>
            </form>
          </div>
        </div>
        <div class="col-xl-3 col-md-6 col-sm-6 col-12 mb-4 mt-2">
          <div class="socialboxfooter">
            <h3>Follow Us</h3>
            <a class="facebook" title="Facebook Link" href="{{ config('settings.facebook_link') }}" target="_blank"><i class="fa fa-facebook"></i></a>
            <a class="twitter" title="Twitter Link" href="{{ config('settings.twitter_link') }}" target="_blank"><i class="fa fa-twitter"></i></a>
            <a class="instagram" title="Instagram Link" href="{{ config('settings.instagram_link') }}" target="_blank"><i class="fa fa-instagram"></i></a>
            <a class="pinterest" title="Pinterest Link" href="{{ config('settings.youtube_link') }}" target="_blank"><i class="fa fa-youtube"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footerbtm">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="copyright">&copy; {{date('Y')}} <a href="{{ url('/') }}" title="Codes Tutors">Codes Tutors.</a> All Rights Reserved.</div>
        </div>
      </div>
    </div>
  </div>
</footer>
