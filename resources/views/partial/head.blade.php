<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="@yield('meta_description')">
<meta name="keywords" content="@yield('meta_keywords')">
<meta name="author" content="">
<meta name="theme-color" content="#364d7d" />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/ico" sizes="32x32" href="{{ asset('assets/images/favicon.ico') }}">
<meta name="google-site-verification" content="PiSTrTCrK4_5DZArK9ROh66extREhmBdZTYqCDpV0RA" />
<title>@yield('title') :: CodesTutors</title>

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
<!-- FontAwesome -->
<link href="{{ asset('assets/fonts/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&family=Poppins:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">
<link rel="preload" href="{{ asset('assets/fonts/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0') }}" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="{{ asset('assets/fonts/svg/icomoon.ttf?7vfwjk') }}" as="font" type="font/ttf" crossorigin>
<!-- Owl Carousel -->
<link rel="stylesheet" href="{{ asset('assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/owl.theme.default.min.css') }}">
<!-- CustomCss -->
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/css/media.css') }}" rel="stylesheet" type="text/css" />