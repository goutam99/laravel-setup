<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Hash;
use App\User;
use App\Category;
use App\SubCategory;
use App\Post;
use App\PostCategory;
use App\PostComments;
use App\Slider;
use App\Cms;
use App\Newsletter;
use Auth;

class HomeController extends Controller
{  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {        
        $data['page_title'] = config('settings.meta_title');
        $data['meta_title'] = config('settings.meta_title');
        $data['meta_keywords'] = config('settings.meta_keywords');
        $data['meta_description'] = config('settings.meta_description');
        $data['tutorials_library'] = Category::where('status',1)->where('parent_id',NULL)->orderBy(DB::raw('sorting = 0, sorting'), 'asc')->get();
        $data['sliders'] = Slider::orderBy(DB::raw('sorting = 0, sorting'), 'asc')->get();

        return view('frontend.home')->with($data);
    }

    public function category($category=null)
    {
        $data['category_details'] = $category_dt = Category::where('category_slug', $category)->first();        
        $data['page_title'] = $category_dt->meta_title;
        $data['meta_title'] = $category_dt->meta_title;
        $data['meta_keywords'] = $category_dt->meta_keywords;
        $data['meta_description'] = $category_dt->meta_description;
        
        return view('frontend.main_category_list')->with($data);
    }

    public function sub_category($category=null, $subcategory)
    {
        //dd(Auth::user());
        $data['category'] = $category_dt = Category::where('category_slug', $category)->first();         
        $data['subcategory_dt'] = $subcategory = SubCategory::where('category_slug', $subcategory)->first();
        $data['page_title'] = $category_dt->category_name;
        $data['meta_title'] = $category_dt->meta_title.' - '.$subcategory->meta_title;
        $data['meta_keywords'] = $category_dt->meta_keywords;
        $data['meta_description'] = $category_dt->meta_description;        
        $data['post'] = ($subcategory->post_categorys)?Post::find($subcategory->post_categorys->first()->ct_posts_id):array();
        //return view('frontend.sub_category_list')->with($data);
        return view('frontend.post_details')->with($data);
    }

    public function post_details($category=null, $subcategory, $post_slug)
    {        
        $data['category'] = $category_dt = Category::where('category_slug', $category)->first();         
        $data['subcategory_dt'] = $subcategory = SubCategory::where('category_slug', $subcategory)->first();
        $data['page_title'] = $category_dt->category_name;
        $data['meta_title'] = $category_dt->meta_title.' - '.$subcategory->meta_title;
        $data['meta_keywords'] = $category_dt->meta_keywords;
        $data['meta_description'] = $category_dt->meta_description;        
        $data['post'] = Post::where('post_slug', $post_slug)->first();
        return view('frontend.post_details')->with($data);
    }

    public function submit_comments(Request $request)
    {
        $redirect_url = $request->input('redirect_uri');    
        if(empty($redirect_url))
        {
            return redirect('/');
        }

        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $data = [
            'secret' => config('services.recaptcha.secret'),
            'response' => $request->get('recaptcha'),
            'remoteip' => $remoteip
          ];
        $options = [
            'http' => [
              'header' => "Content-type: application/x-www-form-urlencoded\r\n",
              'method' => 'POST',
              'content' => http_build_query($data)
            ]
        ]; 
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);
        if ($resultJson->success != true) {
                return back()->withErrors(['captcha' => 'ReCaptcha Error']);
                }
        if ($resultJson->score >= 0.3) {
                //Validation was successful, add your form submission logic here
                //return back()->with('message', 'Thanks for your message!');
        } else {
                return back()->withErrors(['captcha' => 'ReCaptcha Error']);
        }

        if(Auth::user()->id)
        {         
          $validator = Validator::make($request->all(), [
                'comment' => 'required|string|max:5000',   
            ]);
          $user_id = Auth::user()->id;
          
        }else{            
            $validator = Validator::make($request->all(), [
                'comment' => 'required|string|max:5000',  
                'email' => 'required|email|max:250',
                'name' => 'required|alpha_spaces|max:150',
                'website' => 'link|max:250'
            ]);

            
            $password = 'guestuser123456';
            if (Auth::attempt(['email' => $request->input('email'), 'password' => $password])) {
                $request->session()->regenerate();
                $user = Auth::user();
            }else{
                $user = new User;
                $user->name = $request->input('name');
                $user->email = $request->input('email');
                $user->password = Hash::make($password);
                $user->email_verified_at = NULL;
                $user->user_ip = $request->ip();

                $user->save();
                Auth::login($user);
            }
            
            $user_id = $user->id;

        }

        if ($validator->fails()) {
            return redirect($redirect_url.'/#comments-section')
                        ->withErrors($validator)
                        ->withInput();
        }

        $post_id = base64_decode($request->input('post'));
        if(Post::find($post_id))
        {
            $comments_data = array(
                'parent_id' => $request->input('user_reply'),
                'post_id' => $post_id,
                'user_id' => $user_id,
                'rating' => ($request->input('rating'))?:3,
                'comment' => $request->input('comment'),
                'ipaddress' => $request->ip(),
                'created_at' => date('Y-m-d H:i:s')
              );

            PostComments::insert($comments_data);

            return redirect($redirect_url.'/#comments-section')->with('success', 'Commented successfully');
        }
        
    }

    public function delete_comment($id)
    {
        if($id)
        {
            $user_id = Auth::user()->id;
            if($user_id)
            {
                PostComments::where('user_id', $user_id)->where('id', $id)->delete();
                PostComments::where('parent_id', $id)->delete();
                return redirect()->back()->with('success','Comment deleted successfully');
            }
        }
    }
    
    public function pages($slug)
    {
        $data = array();
        $data['cms'] = $cms = Cms::where('slug', $slug)->first();
        $data['page_title'] = $cms->page_name;
        $data['meta_title'] = $cms->meta_title;
        $data['meta_keywords'] = $cms->meta_keywords;
        $data['meta_description'] = $cms->meta_description;
        return view('frontend.pages')->with($data);
    }
    
    public function subscribe_newsletter(Request $request)
    {
        if($request->input('email'))
        {
            if(!Newsletter::where('email', $request->input('email'))->exists())
            {
                Newsletter::insert(['email'=>$request->input('email'), 'user_ip'=>$request->ip(), 'status'=>1, 'created_at'=>date('Y-m-d H:i:s')]);
                return response()->json([
                    'type' => 'success',
                    'message' => 'Thanks for subscribed our newsletter.'               
                ], 200);
            }else{                
                return response()->json([
                    'type' => 'success',    
                    'message' => 'You have already subscribed.'           
                ], 200);
            }
            
        }else{
            return response()->json('',500);
        }
    }
    
    public function search(Request $request)
    {
        $data['q'] = $q = $request->get('q');
        $data['page_title'] = config('settings.meta_title');
        $data['meta_title'] = config('settings.meta_title');
        $data['meta_keywords'] = config('settings.meta_keywords');
        $data['meta_description'] = config('settings.meta_description');
        $result = array();
        $result = Post::with('post_categorys')->where('status', 1)
                  ->when($q, function($query) use ($q){
                      $query->where('meta_title', 'LIKE', '%'.$q.'%')->orWhere('meta_keywords', 'LIKE', '%'.$q.'%')->orWhere('meta_description', 'LIKE', '%'.$q.'%');
                  })->get();
    
        $data['results'] = $result;
        return view('frontend.search')->with($data);
        
    }
    
    public function sitemap()
    {
        return response()->view('sitemap')->header('Content-Type', 'text/xml');
    }
}
